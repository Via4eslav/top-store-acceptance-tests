package base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.open;

public class BaseTest {
    private Properties prop = new Properties();
    private String log4jConfPath = System.getProperty("user.dir") + "/log4j.properties";

    @BeforeMethod
    public void beforeMethod() throws IOException {
        setUpConfig();
        open(baseUrl);
    }

//    @AfterMethod
//    public void tearDown(){
//        WebDriverRunner.getWebDriver().quit();
//    }

    public static void openProductPage(){
        open("/products/k2-balsam-700-ml");
    }

    private void setUpConfig() throws IOException {
        /*Логер добавляет скриншоты к упавшим тестам*/
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));

        /*Позволяет использовать env.properties в корневой директории*/
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/env.properties");
        prop.load(fis);

        /*Для логов*/
        BasicConfigurator.configure();
        PropertyConfigurator.configure(log4jConfPath);

        /*Стандартные настройки перед выполнением тестов. Редактируются в файле env.properties*/
        Configuration.baseUrl = prop.getProperty("URL");
        Configuration.browserSize = prop.getProperty("SIZE");
        Configuration.fastSetValue = Boolean.parseBoolean(prop.getProperty("FAST_VALUE"));
        Configuration.headless = Boolean.parseBoolean(prop.getProperty("HEADLESS"));
        Configuration.timeout = Long.parseLong(prop.getProperty("TIMEOUT"));
        Configuration.savePageSource = Boolean.parseBoolean(prop.getProperty("SOURCE"));

        /*При значении true, методы setValue и val в SelenideElement могут работать как selectOptionByValue, selectRadio
        в зависимости от реального типа элемента управления, определенного тегом элемента.*/
        Configuration.versatileSetValue = Boolean.parseBoolean(prop.getProperty("VALUE"));
    }
}
