package data;

import com.github.javafaker.Faker;

import java.util.Random;

public class UserData {
    static Faker faker = new Faker();

    public static String
            //Данные тестового пользователя
            userLogin = "marchuk.m.wezom@gmail.com",
            userPassword = "111111",

            //Генерирует случайный валидный email адрес. Пример: 6691juston.moore@example.com
            newUserEmail = new Random().nextInt(10010) + faker.internet().safeEmailAddress(),

            //Данные для тестов
            phoneNumber = "999999999",
            userName = "Test Username",
            feedback = "Hi. This is a test comment. Keep calm!";
}
