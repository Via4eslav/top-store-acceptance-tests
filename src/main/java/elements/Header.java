package elements;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import popups.CallbackPopup;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.*;

public class Header {
    public SelenideElement
            logoHeader = $(".page-header__logo [src]"),
            loginIcon = $(".page-header__widgets [data-mfp-src] .header-widget__icon"),
            widgetAccountLogout = $(".header-widget__lk-list .header-widget__lk-list-item:nth-of-type(5) span"),

            callbackButton = $(".page-header__top [data-mfp-src]");

    public static SelenideElement
            loginIconAuth = $(".page-header__widgets [data-toggler-id='userLogin']:nth-of-type(3) .header-widget__icon"),
            loginIconLayer = $(".page-header__widgets [data-toggler-id] [data-toggler-id]");

    //Элементы ЛК. Нужно вынести в необходимый класс
    public SelenideElement
            accountTittle = $(".second-title--lg"),
            exitLink = $(".lk-nav__link--exit span");

    @Step("Кликнуть на иконку входа в личный кабинет")
    public void openLoginPopup(){
        loginIcon.click();
    }

    @Step("Убедиться, что пользователь успешно авторизован")
    public static void assertThatUserIsAuthorized(){
        loginIconAuth.hover();
        loginIconLayer.shouldBe(visible);
    }

    @Step("Убедиться, что пользователь успешно вышел из системы")
    public void assertThatUserIsLogout(){
        loginIcon.shouldBe(visible);
    }
    @Step("Проскроллить к header")
    public void scrollToHeader() {
        logoHeader.scrollTo();
    }

    @Step("В header присутствует логотип")
    public void headerLogoIsPresented(){
        scrollToHeader();
        logoHeader.shouldBe(visible);
        logoHeader.isImage();
    }

    @Step("Открыть окно заказа звонка")
    public CallbackPopup openCallbackPopup(){
        callbackButton.click();
        return new CallbackPopup();
    }

    @Step("Выйти из аккаунта коротким путем")
    public void shortWayToLogout() {
        sleep(2000);
        loginIconAuth.hover();
        loginIconLayer.shouldBe(visible);
        widgetAccountLogout.click();
    }

    @Step("Выйти из аккаунта")
    public void logOutFromAccount(){
//        open(baseUrl + "/account");
        sleep(2000);
        accountTittle.shouldBe(visible);
        exitLink.click();
    }
}
