package elements;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    public SelenideElement
            logoFooter = $(".logo-block--footer [src]");

    @Step("Проскроллить к footer")
    public void scrollToFooter() {
        logoFooter.scrollTo();
    }

    @Step("В footer присутствует логотип")
    public void footerLogoIsPresented(){
        scrollToFooter();
        logoFooter.shouldBe(Condition.visible);
        logoFooter.isImage();
    }
}
