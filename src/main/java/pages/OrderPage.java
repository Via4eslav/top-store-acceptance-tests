package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import popups.RegistrationPopup;

import static com.codeborne.selenide.Selenide.$;

public class OrderPage {

    public SelenideElement
            emailInput = $(By.name("email")),
            phoneInput =$(".regular-form__content .form-group:nth-of-type(2) .form-group__input"),
            nameInput = $(By.name("name")),

            citySelect = $("[data-tabs-container='ch1'] .form-group");

    @Step("Выбрать город")
    public OrderPage chooseCity(){
        $(".js-choice-city-wrap .choices__inner [data-id]").click();
        $(".js-choice-city-wrap .choices__inner [data-id]").selectOption(8);
        return this;

    }

    @Step("Указать номер email адрес")
    public OrderPage enterEmail(String newUserEmail) {
        emailInput.setValue(newUserEmail);
        return this;
    }

    @Step("Указать номер телефона")
    public OrderPage enterPhone(String phoneNumber) {
        phoneInput.setValue(phoneNumber);
        return this;
    }

    @Step("Указать имя")
    public OrderPage enterName(String userName) {
        nameInput.setValue(userName);
        return this;
    }
}
