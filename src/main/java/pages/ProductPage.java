package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class ProductPage {
    public SelenideElement
            buyButton = $x(".//div[@class='gcell']/button/div[@class='button__content']");

    @Step("Нажать кнопку ``Купить``")
    public void addToCart() {
        buyButton.click();
    }
}
