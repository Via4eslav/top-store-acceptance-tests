package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

public class UserAccount {
    public SelenideElement
            accountTittle = $(".second-title--lg");

    @Step("Убедиться, что пользователь успешно зарегистрировался в системе")
    public void assertThatUserIsRegistered(){
        accountTittle.shouldBe(visible);
    }
}
