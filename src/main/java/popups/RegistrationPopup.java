package popups;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class RegistrationPopup {
    public SelenideElement
            emailInput = $("[data-tabs-container='sup'] .form-group:nth-of-type(1) .form-group__input"),
            phoneInput = $("[data-tabs-container='sup'] .form-group:nth-of-type(2) .form-group__input"),
            nameInput = $("[placeholder='Ваше имя']"),
            passwordInput = $("[data-tabs-container] .form-group:nth-of-type(5) .form-group__input"),
            termsCheckBox = $("[for] .checkbox__ins"),
            registerButton = $("[data-tabs-container='sup'] .button__text");

    @Step("Ввести email нового пользователя")
    public RegistrationPopup setNewUserEmail(String newUserEmail){
        emailInput.setValue(newUserEmail);
        return this;
    }

    @Step("Указать корректный номер мобильного телефона")
    public RegistrationPopup enterPhoneNumber(String phoneNumber){
        phoneInput.setValue(phoneNumber);
        return this;
    }

    @Step("Указать имя")
    public RegistrationPopup enterName(String userName){
        nameInput.setValue(userName);
        return this;
    }

    @Step("Ввести пароль")
    public RegistrationPopup setPassword(String userPassword){
        passwordInput.setValue(userPassword);
        return this;
    }

    @Step("Отметить чек-бокс о согласии с правилами")
    public void agreeWithTerms(){
        termsCheckBox.click();
    }

    @Step("Нажать кнопку регистрации")
    public void clickRegistrationButton(){
        registerButton.click();
    }
}
