package popups;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CallbackPopup {
    public SelenideElement
            nameInput = $("[method] .form-group:nth-of-type(1) .form-group__input"),
            phoneInput = $("[method] .form-group:nth-of-type(2) .form-group__input"),
            messageArea = $("textarea"),
            submitButton = $("[method] .button__content");

    @Step("Указать имя пользователя")
    public CallbackPopup setName(String userName){
        nameInput.setValue(userName);
        return this;
    }

    @Step("Указать номер телефона")
    public CallbackPopup setPhone(String phoneNumber){
        phoneInput.setValue(phoneNumber);
        return this;
    }

    @Step("Оставить комментарий")
    public CallbackPopup writeFeedback(String feedback){
        messageArea.setValue(feedback);
        return this;
    }

    @Step("Отправить форму заказа звонка")
    public void submitCallbackForm(){
        submitButton.click();
    }
}
