package popups;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.*;

public class AssertionPopups {
    public static SelenideElement succesPopup = $(".mfp-content");

    @Step("Появилось сообщение об успешно совершонном действии")
    public static void assertSuccessAction(){
        succesPopup.shouldBe(visible);
    }
}
