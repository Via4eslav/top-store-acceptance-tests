package popups;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

public class CartPopup {
public SelenideElement
        cartPopup = $(".cart"),
        orderButon = $(".cart__buttons a .button__content");

    @Step("Появилось всплывающее окно корзины")
    public CartPopup assertCartDisplayed() {
        cartPopup.shouldBe(visible);
        return this;
    }

    @Step("Нажать кнопку \"Оформить заказ\" \"")
    public void clickOrderBUtton(){
        orderButon.click();
    }
}
