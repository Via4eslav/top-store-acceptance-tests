package popups;

import com.codeborne.selenide.SelenideElement;
import elements.Header;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LoginPopup extends Header {
    public SelenideElement
            authPopup = $(".tabs-inited"),
            loginInput = $("[data-tabs-container='sin'] .form-group:nth-of-type(1) .form-group__input"),
            passwordInput = $("[data-tabs-container='sin'] .form-group:nth-of-type(2) .form-group__input"),
            enterButton = $("[data-tabs-container='sin'] .button__content"),

            registrationLink = $("[data-tabs-container='sin'] [data-tabs-button='sup'] span");

    //Необходим для тестирования неавторизованным пользователем методов добавления товаров в избранное и т.д.
    @Step("Проверяем появление модального окна авторизации")
    public void assertLoginPopupDisplayed(){
        authPopup.shouldBe(visible);
    }

    @Step("Ввести логин")
    public LoginPopup setLogin(String userLogin){
        loginInput.setValue(userLogin);
        return this;
    }

    @Step("Ввести пароль")
    public LoginPopup setPassword(String userPassword){
        passwordInput.setValue(userPassword);
        return this;
    }

    @Step("Нажать кнопку входа в личный кабинет")
    public void clickEnterToAccount(){
        enterButton.click();
    }

    @Step("Переключиться на форму регистрации")
    public RegistrationPopup switchToRegistrationWindow(){
        registrationLink.click();
        return new RegistrationPopup();
    }
}
