package tests;

import base.BaseTest;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import pages.OrderPage;
import pages.ProductPage;
import popups.CartPopup;

import static com.codeborne.selenide.Selenide.sleep;
import static data.UserData.*;

public class AddToCart extends BaseTest {
    private ProductPage productPage = new ProductPage();
    private CartPopup cartPopup = new CartPopup();

    @Feature("Добавить товар в корзину со страницы продукта")
    @Test
    public void addFromProductPage(){
        openProductPage();
        productPage.addToCart();

        cartPopup
                .assertCartDisplayed()
                .clickOrderBUtton();

        new OrderPage()
                .enterEmail(newUserEmail)
                .enterPhone(phoneNumber)
                .enterName(userName)
                .chooseCity();
        sleep(10000);
    }
}
