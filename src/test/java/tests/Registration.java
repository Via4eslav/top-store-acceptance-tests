package tests;

import base.BaseTest;
import elements.Header;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import pages.UserAccount;
import popups.LoginPopup;
import popups.RegistrationPopup;

import static data.UserData.*;

public class Registration extends BaseTest {
    private Header header = new Header();
    private LoginPopup loginPopup = new LoginPopup();
    private RegistrationPopup registrationPopup = new RegistrationPopup();

    @Feature("Регистрация нового пользователя")
    @Test
    public void newUserCanRegister(){
        header.openLoginPopup();
        loginPopup.switchToRegistrationWindow();

        registrationPopup
                .setNewUserEmail(newUserEmail)
                .enterPhoneNumber(phoneNumber)
                .enterName(userName)
                .setPassword(userPassword)
                .agreeWithTerms();
        registrationPopup.clickRegistrationButton();
        new UserAccount().assertThatUserIsRegistered();

        header.logOutFromAccount();
        header.assertThatUserIsLogout();
    }
}
