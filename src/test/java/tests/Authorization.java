package tests;

import base.BaseTest;
import elements.Header;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import popups.LoginPopup;

import static data.UserData.*;
import static elements.Header.*;

public class Authorization extends BaseTest {
    private Header header = new Header();
    private LoginPopup loginPopup = new LoginPopup();

    @Feature("Авторизация существующего пользователя")
    @Test
    public void existingUserCanLogin(){
        header.openLoginPopup();

        loginPopup
                .setLogin(userLogin)
                .setPassword(userPassword);
        loginPopup.clickEnterToAccount();

        assertThatUserIsAuthorized();

        loginPopup.shortWayToLogout();

        header.assertThatUserIsLogout();


    }
}
