package tests;

import base.BaseTest;
import elements.Footer;
import elements.Header;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

public class LogoIsPresented extends BaseTest {
    @Feature("Логотип присутствует в HEADER и FOOTER")
    @Test
    public void isLogoPresentedAtPage(){
        new Header().headerLogoIsPresented();
        new Footer().footerLogoIsPresented();
    }
}
