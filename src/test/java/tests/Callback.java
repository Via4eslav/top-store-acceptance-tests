package tests;

import base.BaseTest;
import elements.Header;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import popups.CallbackPopup;

import static data.UserData.*;
import static popups.AssertionPopups.*;

public class Callback extends BaseTest {
    private CallbackPopup callback = new CallbackPopup();

    @Feature("Заказ звонка")
    @Test
    public void userCanOrderCallback(){
        new Header().openCallbackPopup();

        callback
                .setName(userName)
                .setPhone(phoneNumber)
                .writeFeedback(feedback);
        callback.submitCallbackForm();

        assertSuccessAction();
    }
}
