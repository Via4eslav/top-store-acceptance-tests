package bdd_tests;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class Пользователь_ {
    public static SelenideElement
            nameInput = $("[method] .form-group:nth-of-type(1) .form-group__input"),
            phoneInput = $("[method] .form-group:nth-of-type(2) .form-group__input"),
            messageArea = $("textarea"),
            submitButton = $("[method] .button__content"),
            callbackButton = $(".page-header__top [data-mfp-src]"),
            succesPopup = $(".mfp-content");

    @Step("Открыть форму обратного звонка")
    public static void кликает_Кнопку_Обратной_Связи_В_Шапке_Сайта() {
        callbackButton.click();
    }

    @Step("Указать имя пользователя")
    public static void указывает_Своё_Имя  (String имя){
        nameInput.setValue(имя);
    }

    @Step("Указать номер телефона")
    public static void указывает_Свой_Номер_Телефона(String номер){
        phoneInput.setValue(номер);
    }

    @Step("Оставить комментарий")
    public static void оставляет_Комментарий(String текст){
        messageArea.setValue(текст);
    }

    @Step("Отправить форму заказа звонка")
    public static void отсылает_Форму(){
        submitButton.click();
    }

    @Step("Появилось сообщение об успешно совершонном действии")
    public static void проверяет_Что_Форма_Отправлена(){
            succesPopup.shouldBe(visible);
    }
}
